import React, { useState, useEffect } from "react";

import { MovieCard } from "../components/MovieCard";
import { GenreResponseProps, MovieProps } from "../components";

import { api } from "../services/api";

interface ContentPros {
  id: Number;
}

export function Content(props: ContentPros) {
  const { id } = props;
  const [movies, setMovies] = useState<MovieProps[]>([]);
  const [selectedGenre, setSelectedGenre] = useState<GenreResponseProps>(
    {} as GenreResponseProps
  );

  useEffect(() => {
    api.get<MovieProps[]>(`movies/?Genre_id=${id}`).then((response) => {
      setMovies(response.data);
    });

    api.get<GenreResponseProps>(`genres/${id}`).then((response) => {
      setSelectedGenre(response.data);
    });
  }, [id]);

  return (
    <>
      <header>
        <span className="category">
          Categoria:<span> {selectedGenre.title}</span>
        </span>
      </header>

      <main>
        <div className="movies-list">
          {movies.map((movie) => (
            <MovieCard
              key={movie.imdbID}
              title={movie.Title}
              poster={movie.Poster}
              runtime={movie.Runtime}
              rating={movie.Ratings[0].Value}
            />
          ))}
        </div>
      </main>
    </>
  );
}
